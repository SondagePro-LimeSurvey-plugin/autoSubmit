/*  This file is part of Lime Survey Plugin : autoSubmit */

$(document).ready(function(){
	if($("[id^='question']").length==1){
		$('body').addClass('onequestion');
	}
	$("table.questions-list").each(function(){
		if($(this).find(".radio-list").length==1){
			$(this).addClass('onesubquestion');
		}
	});
	$(".onequestion .list-dropdown select").change(function(){
		if(bAutoMove && $(this).val()!="" && $(this).val()!="-oth-"){
			$("#movenextbtn,#movesubmitbtn").delay(300).click();
		}
	});
	$(".onequestion .list-radio input[type=radio],.onequestion .yes-no input[type=radio]").click(function(){
		if(bAutoMove && $(this).val()!="" && $(this).val()!="-oth-"){
			$("#movenextbtn,#movesubmitbtn").delay(300).click();
		}
	});
	$(".onequestion .onesubquestion input[type=radio]").click(function(){
		if(bAutoMove && $(this).val()!="" && $(this).val()!="-oth-"){
			$(this).trigger("click");
			$("#movenextbtn,#movesubmitbtn").delay(300).click();
		}
	});
});

